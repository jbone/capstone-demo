<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text\html; charset=ISO-8859-1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
	<title>Dog Page</title>
</head>
<body>
	<h1 class="text-center">
		${Greet}
	</h1>
	<table class="table">
		<caption>All About Dogs!</caption>
		<tr>
		<th>Name</th>
		<th>Type</th>
		<th>Color</th>
		</tr>
		<c:forEach var="dog" items="${Dogs}">
			<tr>
			<td><c:out value="${dog.name}"/></td>
			<td><c:out value="${dog.type}"/></td>
			<td><c:out value="${dog.color}"/></td>
			</tr>
		</c:forEach>
	</table>

<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
</body>
</html>