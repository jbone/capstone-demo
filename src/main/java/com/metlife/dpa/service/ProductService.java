package com.metlife.dpa.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.metlife.dpa.model.Product;

@Service
public class ProductService implements IProductService {
	
	
	public List<Product> getProducts(){
		List<Product> products = new ArrayList<Product>();
		products.add(new Product("Backpack", BigDecimal.valueOf(79.99)));
		products.add(new Product("Shirt", BigDecimal.valueOf(49.99)));
		products.add(new Product("Running Shoes", BigDecimal.valueOf(89.99)));
		return products;
	}
}
