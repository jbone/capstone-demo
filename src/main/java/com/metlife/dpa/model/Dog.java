package com.metlife.dpa.model;

public class Dog{
	private String name;
	private String type;
	private String color;

	public Dog(String name, String type, String color){
		this.name = name;
		this.type = type;
		this.color = color;
	}

	public String getName(){
		return this.name;
	}

	public String getType(){
		return this.type;
	}

	public String getColor(){
		return this.color;
	}
}