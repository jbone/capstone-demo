package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.dpa.model.Dog;

@Controller
public class DogController {
	private Dog[] dogs = {new Dog("Blueberry", "Huskey","Red"), new Dog("Rasberry","Wolf","Green"), new Dog("Apple", "Laborador", "Blue")};
	@RequestMapping(value="/Dog", method = RequestMethod.GET)
	public String displayTable(Model model) {
        model.addAttribute("Dogs", dogs);
        return "dog";
    }

    @RequestMapping(value="/Dog", method=RequestMethod.POST)
    public String searchDogs(@ModelAttribute Dog input, Model model){
    	model.addAttribute("greeting", "Hello I am a ");
    	return "result";
    }
}