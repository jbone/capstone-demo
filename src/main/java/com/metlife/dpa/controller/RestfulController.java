package com.metlife.dpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.dpa.model.Product;
import com.metlife.dpa.service.IProductService;


@RestController
public class RestfulController {
	
	@Autowired
	IProductService products;

	@RequestMapping("/products")
	public List<Product> getProducts(){

		return products.getProducts();
	}
	
	@RequestMapping(value="/products", method=RequestMethod.POST)
	public Product addProduct(@RequestBody Product product){
		return product;
	}

}
