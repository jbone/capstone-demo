package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.dpa.model.Greeting;

@Controller
public class HomeController {

	@RequestMapping(value="/")
	public String sayHello(Model model){
		model.addAttribute("greeting", "Hello world!");
		return "index";
	}
	
    @RequestMapping(value="/greeting", method=RequestMethod.GET)
    public String greetingForm(Model model) {
        model.addAttribute("formGreeting", new Greeting());
        return "greeting";
    }

    @RequestMapping(value="/greeting", method=RequestMethod.POST)
    public String greetingSubmit(@ModelAttribute Greeting greeting, Model model) {
    	System.out.println(greeting.getContent());
        model.addAttribute("greeting", greeting);
        return "result";
    }
	
}
